<?php

class html {
    public static $specialAttributes = array(
        'async' => true,
        'autofocus' => true,
        'autoplay' => true,
        'checked' => true,
        'controls' => true,
        'declare' => true,
        'default' => true,
        'defer' => true,
        'disabled' => true,
        'formnovalidate' => true,
        'hidden' => true,
        'ismap' => true,
        'loop' => true,
        'multiple' => true,
        'muted' => true,
        'nohref' => true,
        'noresize' => true,
        'novalidate' => true,
        'open' => true,
        'readonly' => true,
        'required' => true,
        'reversed' => true,
        'scoped' => true,
        'seamless' => true,
        'selected' => true,
        'typemustmatch' => true,
    );
    public static $selfClosingTags = array(
        'img' => true,
        'meta' => true,
    );

    static function tag($tag, array $attributes, $content, $close = true, $echo = false) {
        $result = static::open($tag, $attributes, false);
        if(strlen($content) || $close || !isset(static::$selfClosingTags[$tag]))
            $result .= $content . static::close($tag, false);
        if($echo)
            echo $result;
        return $result;
    }

    static function open($tag, array $attributes = array(),$echo = false) {
        $result =  '<' . $tag . static::attributes($attributes, false) . '>';
        if($echo)
            echo $result;
        return $result;
    }

    static function close($tag, $echo = false) {
        $result =  '</' . $tag . '>';
        if($echo)
            echo $result;
        return $result;
    }

    static function attributes(array $attributes, $echo = false) {
        $result = '';
        if(!empty($attributes)) {
            foreach($attributes as $key => $value) {
                if(isset(static::$specialAttributes[$key])) {
                    if($value) {
                        $result .= ' ' . $key;
                    }
                } else if($value !== null) {
                    if(is_array($value) || is_object($value)) {
                        $value = json_encode($value);
                    } else {
                        $value = htmlspecialchars($value);
                    }
                    $result .= ' ' . $key . '=\'' . $value . '\'';
                }
            }
        }
        if($echo)
            echo $result;
        return $result;
    }

    static function link($text, $url = '#', array $attributes = array(), $echo = false) {
        $attributes['href'] = $url;
        return static::tag('a', $attributes, $text, true, $echo);
    }

    static function mailto($text, $email = null, array $attributes = array(), $echo = false) {
        if($email === null)
            $email = $text;
        return static::link($text, 'mailto:' . $email, $attributes, $echo);
    }

    static function image($src, $alt = '', array $attributes = array(), $echo = false) {
        $attributes['src'] = $src;
        $attributes['alt'] = $alt;
        return static::tag('img', $attributes, null, false, $echo);
    }

    static function bgimage($src, $alt = '', array $attributes = array(), $echo = false) {
        $png = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
        $attributes['style'] = 'background-image:url(' . $src . ')';
        return static::image($png, $alt, $attributes, $echo);
    }

    static function placeholder($width = 320, $height = 240) {
        return static::image('http://placehold.it/' . $width . 'x' . $height);
    }

    static function kitten($width = 320, $height = 240) {
        if(getenv('LOCAL_ENV') == 'localdev') {
            return static::image('http://placekitten.com/g/' . $width . '/' . $height);
        }
        return static::placeholder($width, $height);
    }
}
